<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SensorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sensors')->insert([
            'hw_id' => '123456',
            'name' => 'Temperature',
            'value' => (0),
            'user_id' => (1),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
