<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    //
    protected $primaryKey = 'hw_id';
    protected $fillable = ['hw_id', 'name', 'value'];
}
