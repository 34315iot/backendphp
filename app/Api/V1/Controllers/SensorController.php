<?php
 
namespace App\Api\V1\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\Sensor;
use Dingo\Api\Routing\Helpers;
use DB;
use Symfony\Component\HttpKernel\Exception\HttpException;
 



class SensorController extends Controller
{
    use Helpers;
    

 
    public function index()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        return $currentUser
            ->sensors()
            ->orderBy('created_at','DESC')
            ->get()
            ->toArray();
    }
 
    public function store(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
 

        if($currentUser->sensors()->find($request->get('hw_id')))
            return $this->response->error('sensor_duplication', 500);

        $sensor = new Sensor;




        
 
        $sensor->hw_id = $request->get('hw_id');
        $sensor->name = $request->get('name');
        $sensor->value = 0;

        $temp = (int)$request->get('hw_id');
      
        if($temp< 500000){
           $type = 'temp'; 
       }else{
        $type = 'switch';
       }

        $sensor->type = $type;



        
 

 
        if($currentUser->sensors()->save($sensor))
            //return $this->response->created();
            return response()->json([
                'type' => $type
            ], 201);
        else
            return $this->response->error('could_not_add_sensor', 500);
    }

    public function show(Request $request)
    {
    	$currentUser = JWTAuth::parseToken()->authenticate();

    	$id = $request->get('id');

    	$sensor = $currentUser->sensors()->find($id);

    	if(!$sensor)
    		throw new NotFoundHttpException;
    	
    	return $sensor;	
    }

    public function update(Request $request)
    {
    	$currentUser = JWTAuth::parseToken()->authenticate();

    	$id = $request->get('id');

    	$sensor = $currentUser->sensors()->find($id);

    	if(!$sensor)
    		throw new NotFoundHttpException;

    	$sensor->fill($request->all());

    	if($sensor->save())
    		return $this->response->noContent();
    	else
    		return $this->response->error('could_not_update_sensor', 500);

    }

    public function remove(Request $request)
    {
    	$currentUser = JWTAuth::parseToken()->authenticate();

    	$id = $request->get('id');

    	$sensor = $currentUser->sensors()->find($id);

    	if(!$sensor)
    		throw new NotFoundHttpException;

    	$sensor->fill($request->all());

    	if($sensor->delete())
    		return $this->response->noContent();
    	else
    		return $this->response->error('could_not_delete_sensor', 500);

    }

    // Update sensor value from app/user 
    public function valueupdate(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $id = $request->get('id');
        $value = $request->get('value');

        $sensor = $currentUser->sensors()->find($id);

        if(!$sensor)
            throw new NotFoundHttpException;

        $sensor->value = $value;
        $sensor->save();


        return response()->json([
            'status' => 'ok'
        ], 201);


    }







    // used by gateway

    //TODO:
    // - gateway authentication
    public function gatewayUpdate(Request $request){


        $id = $request->get('id');
        $value = $request->get('value');
        //$hw_id = $request->get('hw_id');

        $sensor = Sensor::find($id); // Uses primary key in Sensor.php
        if($sensor) {
            $sensor->value = $value;
            $sensor->save();
        }
        else
            return $this->response->error('could_not_find_sensor',500);


        return response()->json([
            'status' => 'ok'
        ], 201);


        

    }

    public function getValueUpdate(Request $request)
    {
        $id = $request->get('id');
        $sensor = Sensor::find($id);
        if($sensor)
            return $sensor->value;
        else
            return $this->response->error('could_not_find_sensor',500);

    }



    /*public function gatewayUpdate(Request $request)
    {

    	$id = $request->get('id');

    	//$sensor = sensors()->find($id);

    	
    	//$sensors = DB::table('sensors')->get();
    	$sensors = select * from sensors where id = 2;

		foreach ($sensors as $sensor)
		{
    		echo "$sensor->id";
		}

    	//$sensor = DB::table('sensors')->where('id', '2');

    	//if(!$sensor)
    	//	throw new NotFoundHttpException;

    	/*$sensor->fill($request->all());

    	if($sensor->save())
    		return $this->response->noContent();
    	else
    		return $this->response->error('could_not_update_sensor', 500);*/
    	
    	//return json_encode($sensor);

   // }


 
 
 
/*
    public function update(Request $request, $id, $value)
    {
        $sensor = DB::table('sensors)'->find($id);
        if(!$sensor)
            throw new NotFoundHttpException;
 
        $sensor->fill($value)->value();
 
        if($sensor->save())
            return $this->response->noContent();
        else
            return $this->response->error('Could_not_update_sensor_value, 500');
    }
*/
 
 
 
}