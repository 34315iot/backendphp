<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to this item is only for authenticated user. Provide a token in your request!'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);

        $api->get('sensor','App\\Api\\V1\\Controllers\\SensorController@index');
        $api->post('sensor/store','App\\Api\\V1\\Controllers\\SensorController@store');
        $api->get('sensor/show','App\\Api\\V1\\Controllers\\SensorController@show');
        $api->put('sensor/update','App\\Api\\V1\\Controllers\\SensorController@update');
        $api->delete('sensor/remove','App\\Api\\V1\\Controllers\\SensorController@remove');
        $api->get('sensor/valueupdate', 'App\\Api\\V1\\Controllers\\SensorController@valueupdate');


        //$api->resource('sensor/','App\\Api\\V1\\Controllers\\SensorController');
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });

    $api->get('test', function() {
        return response()->json([
            'message' => 'test aj'
        ]);
    });

    $api->put('gatewayUpdate','App\\Api\\V1\\Controllers\\SensorController@gatewayUpdate');
    $api->get('getValue', 'App\\Api\\V1\\Controllers\\SensorController@getValueUpdate');


});
